---
title: "École de Musique de Cazères"
date: 2022-04-06T10:47:42+02:00
draft: false
---

- [Tarifs École de musique](#tarifs-école-de-musique)
- [Présentation de l'école](#présentation-de-lécole)
	- [Un petit historique](#un-petit-historique)
	- [Objectifs](#objectifs)
		- [Les instruments étudiés sont](#les-instruments-étudiés-sont)
		- [A ces pratiques instrumentales s'ajoutent les cours collectifs](#a-ces-pratiques-instrumentales-sajoutent-les-cours-collectifs)
- [L'équipe enseignante](#léquipe-enseignante)
	- [Marc BOER, enseignant guitare et basse](#marc-boer-enseignant-guitare-et-basse)
	- [Dominique DUCLOZ, enseignante piano](#dominique-ducloz-enseignante-piano)
	- [Denis MINIER, enseignant batterie et percussions](#denis-minier-enseignant-batterie-et-percussions)
	- [Caroline BONNAUD , enseignante  chant (et chant choral)](#caroline-bonnaud--enseignante--chant-et-chant-choral)
	- [Elizabeth OPIE, enseignante clarinette, saxophone et flute traversière.](#elizabeth-opie-enseignante-clarinette-saxophone-et-flute-traversière)
- [Contact & Accès](#contact--accès)
	- [Les salles](#les-salles)

## Tarifs École de musique

> Bonne nouvelle : La bourse pour les jeunes musiciens est destinée aux moins de 18 ans au 30/09 de l’année en cours, résidant en Haute-Garonne, issus d’un foyer fiscal dont le quotient familial CAF ou MSA est inférieur ou égal à 640 €. Le Conseil départemental 31 prend en charge la totalité des frais d’inscription pour l’apprentissage d’un instrument dans le cadre d’un cursus avec formation musicale. Un instrument peut être mis à disposition si besoin.
>
> Pour bénéficier de la bourse, la famille remplit le formulaire d’inscription (disponible en ligne sur le site : haute-garonne.fr/aide/bourses-aux-jeunes-musiciens) et le retourne impérativement avec les pièces justificatives avant le 30 octobre au Conseil départemental 31.
>
> **Renseignements :**
> - Direction des arts vivants et visuels,
> - 7 rue Chalande / 31000 Toulouse
> - 05 34 45 58 30 / contact.dav@cd31.fr.

## Présentation de l'école

### Un petit historique

- 1981 : Création de l’activité musique à la MJC de Cazères (piano)
- 28/09/1986 : Naissance de l’école de musique de Cazères sous la forme d’une association loi 1901 (piano, guitare, flûte, batterie)
- 1996 : Evolution des activités (piano, guitare, batterie, chant, éveil, formation musicale, violon, saxophone, flûte).
- 15/10/1998 : Evolution des statuts : Changement de nom de l’école qui devient l’école
- intercommunale de musique des petites Pyrénées (EIMPP),
- 1998 : Apparition des premiers groupes
- Création de cours de djembé
- 2004 : Formation musicale obligatoire
- Mise en place d’examens internes de niveaux
- Participation aux examens départementaux (BMD)

### Objectifs

Les objectifs de l’association sont à la fois simples et ambitieux :

-permettre à tous ceux qui le souhaitent, petits et grands, de pratiquer leur passion encadrés par des professionnels. Cette pratique instrumentale est individuelle dans un premier temps puis rapidement après quelques mois d’expérience les élèves sont réunis :

*en ateliers où ils étudient ensemble les mêmes morceaux

*en groupes avec lequel ils se produisent sur une scène lors des manifestations organisées par l’école de musique ou dans d’autres occasions.

#### Les instruments étudiés sont


- le piano classique,
- le piano jazz, musique actuelle, travail des grilles d’accord- pour accompagnement chant
- la guitare (classique, jazz, flamenco)
- la guitare électrique et basse,
- la batterie,
- le chant,
- la clarinette,
- le saxophone,

#### A ces pratiques instrumentales s'ajoutent les cours collectifs

- Formation musicale
- Groupe de musique actuelle enfant/ados/adultes
- Percussions
- Chorale
- Éveil Musical

## L'équipe enseignante

### Marc BOER, enseignant guitare et basse

![](images/2022-04-06-10-08-48.png)

La guitare m’arrive dans les mains à 12 ans. Une révélation.
Bien que ce soit un modèle pour gaucher (la guitare appartenant
à mon beau père), j’accroche. Quelques gammes, cordes,
accords plus tard, je remets le tout à l’endroit en m’en achetant
une pour droitier.
Progressivement, je délaisse le rock pour me tourner vers une musique plus jazz et métissée. Je prends des cours de guitare jazz,vais ensuite DEUG de musicologie, pour finalement me payer un an à l’école de jazz du CIM en classe de guitare avec Pierre Cullaz. Il me propose d’y être assistant ce qui me permet de compléter ma formation (2 ans en guitare jazz et 2 ans en arrangement et orchestration jazz)  tout en y dirigeant des ateliers de groupe. Parallèlement, je commence à donner des cours et forme divers projets autours de la musique jazz. Le CIM finit par fermer. Je continue d’apprendre, en prenant quelques cours, pour un langage plus moderne, avec Malo Valois, mais surtout au travers des différentes expériences musicales auxquelles je participe et musiciens que j’y croise. Période riche d’activités, je m’intéresse sur un plan plus personnel au flamenco et à la guitare sud-américaine. Plaisirs boisés et saveurs de cyprès…quelques années passent, je quitte la banlieue morose pour la ville rose et intègre l’équipe de l’EIMPP en 2004.

### Dominique DUCLOZ, enseignante piano

![](images/2022-04-06-10-10-44.png)

**Parole donnée aux élèves et parents…**

”Une méthode originale et libre qui n’impose pas aux enfants.

Pour les petits, une façon amusante d’apprendre le piano sous forme de jeu avec un parent.

” Professeur atypique ayant su se détacher de sa formation classique Pour jouer avec ses propres touches d’apprentissage.

Méthode basée sur la patience et sur une attention adaptée au rythme de chacun. Méthode libre et douce n’excluant pas l’exigence d’un travail personnel rigoureux et responsable. Et pour finir Dominique et une maman pour tous ses élèves

”Domi s’adapte au personnalité de chaque enfant.

J ‘admire en tant que maman et enseignante sa patience et son énergie. Enseignante efficace aussi pour la structure de son cours de 30 minutes. On se rend vite compte qu’elle enseigne avec son cœur, c’est un plaisir. Domi est une enseignante passionnée qui aime son métier et cela se voit!

” détecte et valorise les talents, très à l’écoute de l’attente de l’élève, exigences mesurées.

”Méthode d’apprentissage spécifique adaptée pour chaque tranche d’âge Domi est une prof passionnée de pédagogie. Et c’est une chance que Domi puisse nous ouvrir sur tous les styles musicaux : classique (avec la rigueur de la technique) musique actuelle et une ouverture sur le jazz et l’impro.

Ses élèves la sente tellement motivée qu’ils se sentent obligés de bosser !

**Groupes enfants et ado** : la musique est un partage, et l’école de musique de Cazères est l’une des rares écoles qui a su dès le début de son existence (1982); développer et valoriser les groupes de musiques actuelles. Depuis presque 40 ans ; beaucoup de groupes se sont formés certains ont réussi des concours départementaux; d’autres sont devenus semi-professionnel. Tous se rendent compte de la chance qu’ils ont eu de pouvoir faire partie d’un groupe dès l’âge de 9 ans …activité qui normalement (pour certains privilégiés) se pratique avec des potes mais pas avant 17\19 ans.

Les groupes sont encadrés par un prof, ce dernier leur apprend à travailler ensemble, à se servir d’une sono et pour les groupes d’ado le prof leur apprend à être autonome en les laissant de temps en temps répéter seuls.

---

J’ai commencé la musique à 7 ans ; par 2 ans de solfège, obligatoire avant de commencer son instrument.
J’ai eu comme 1er professeur quelqu’un de très sévère, ancien militaire de carrière.
Lorsque nous n’avions pas assez travaillé, il nous enfermait dans son grenier, sans électricité ni chauffage et nous devions travailler sur le piano, éclairés par une seule bougie, jusqu’à ce qu’il revienne nous chercher, 1 ou 2 heures plus tard.
Mais….j’étais sa meilleure élève et je n’ai connu le grenier qu’une seule fois en 3 ans de cours avec lui. Mon frère et ma soeur y sont montés bien plus souvent (petite anecdote, nous sommes tous les trois prof de piano).
Ensuite, je suis entée au conservatoire de Chambéry.
J’ai eu comme professeur quelqu’un de formidable, qui exerçait sa pédagogie avec beaucoup de finesse.
J’ai très vite su que je ne serai pas concertiste (pas assez douée). Ce qui m’intéressait, c’était l’enseignement et depuis cette rencontre, j’ai toujours été à la recherche de ce “petit truc subtil” qui donne envie à n’importe quel élève d’aimer son instrument et de le travailler.
A 13 ans, j’ai annoncé à mes parents que je serai prof de piano.
J’ai suivi tout le cursus du conservatoire, obtenu mon diplôme de fin d’études et une 2eme médaille en piano, solfège et musique de chambre.
De 18 à 20 ans, j’ai eu la chance d’enseigner au conservatoire (piano et solfège).
Suite à mon mariage, je suis venue vivre dans la région et j’ai trouvé un poste à l’école municipale de Muret.
J’habitais à Gouzens et à cette époque (1981), il n’y avait aucune école de musique entre Saint-Gaudens et Muret.
Avec beaucoup d’ardeur et l’aide des municipalités, je me suis démenée pour monter les écoles de Montesquieu- Volvestre, Cazères, l’école de Mirés Vincent à Muret et enfin celle de Boussens/Saint-Martory.
En parallèle, je suis rentrée dans diverses formations (orchestres de bal musette, quartet de jazz, duo avec chanteurs….).
Et là, je me suis rendue compte que ma formation très classique ne suffisait pas. Je découvrais les grilles d’accords à l’age de 20 ans, après 12 ans de piano !
Très vite j’ai su que je donnerai dès le départ à mes élèves, une ouverture sur le jazz, la variété et le classique.
J’ai fait le choix dans ma profession, de préférer les petites écoles de musique, de taille plus humaines dans lesquelles la liberté d’expression est favorisée. Par 3 fois, j’ai refusé un poste de fonctionnaire dans les écoles de musique municipales, malgré le très bon salaire à la clé, les 20 heures hebdomadaires et l’absence de bénévolat.
A mes yeux, l’école de Cazères est le reflet de ce que devrait être toute école de musique. Nous sommes l’une des rares écoles qui voit ses élèves adolescents continuer à venir prendre des cours. Même les jeunes adultes sont toujours là. Nous travaillons beaucoup en musique d’ensemble, sous forme d’ateliers ou de groupes (variété, rock, jazz et classique).

Après 30 ans d’enseignement, je pense avoir affiné ma pédagogie pour être avec mes élèves “juste assez” exigeante, tout en étant proche d’eux.

### Denis MINIER, enseignant batterie et percussions

![](images/2022-04-06-10-12-09.png)

Il n’y a pas de vie sans rythme.
Il suffit pour s’en persuader d’écouter le monde qui nous entoure: rythme de pas dans la rue, d’un pic vert tapant contre son arbre, d’un oiseau qui chante, d’un cheval dont le pas le trot ou le galop ne produisent pas le même rythme, de femmes africaines pilant le mil, d’un moteur ou d’un clignotant de voiture, etc etc…ou tout simplement son cœur.
Cette passion du rythme, je l’ai découverte à 13 ans lorsque mon frère a ramené une batterie à la maison : ce fut pour moi une véritable révélation. Quelques années plus tard, j’ai commencé à étudier l’instrument dans une mjc, puis à l’école Agostini Toulouse, afin d’approndir ma connaissance du rythme et de la musique.
Après avoir partagé cette passion avec d’autres musiciens en groupe, en quintet jazz et en orchestre, j’ai décidé de la transmettre aux autres par le biais de l’enseignement. Depuis maintenant plusieurs années, j’enseigne dans différentes écoles de musique – Sainte Foy d’Aigrefeuille, Muret et Cazères, où se côtoient des musiciens de tous âges pianistes, guitaristes, chanteurs, flûtistes, violonistes, bassistes et bien sur… batteurs.
Alors si vous avez envie de vous plonger dans l’univers du rythme au travers de la batterie, quel que soit votre âge et votre parcours musical, n’hésitez pas à me contacter ; cela sera avec un réel plaisir que je vous aiderai dans votre démarche musicale.

### Caroline BONNAUD , enseignante  chant (et chant choral)

![](images/2022-04-06-10-12-53.png)

Professeur de chant et chanteuse
Après des études aux conservatoire et à la fac de musicologie de Poitiers, elle commence une carrière de musicienne et chanteuse dans le domaine du théâtre, de la musique et de la chanson et travaille auprès de différentes compagnies.
Depuis 2004, elle donne des cours de chant lyrique et dirige des chorales, elle assure également le coaching de jeunes musiciens de variété et propose régulièrement des stages de lecture.

### Elizabeth OPIE, enseignante clarinette, saxophone et flute traversière.

Prof de Clarinette / Saxophone / Flute Traversière, pratiquant toutes les disciplines : Classique / Blues / Jazz / Musique du Monde / Folk / Pop Rock. J’enseigne aussi le développement des compétences pour les ensembles et surtout la musicalité ( la musi-qualité ! ).

La méthode est adapté à chaque élève et très ludique. 

Apprendre à jouer un instrument de musique n’est pas un loisir facile. Par contre le plaisir de pouvoir jouer et partager la musique surtout en groupe est immense. Je me dispose à vous motiver, vous guider , vous rendre l’apprentissage de l’instrument et les compétences musicales à développer compréhensibles et réalisables. 

Sur les traces musicales de sa mère, Elizabeth apprend et interprète la musique sur scène depuis son plus jeune âge.

Elle joua dans divers ensembles de musique classique pour juniors, jeunes et adultes, orchestres et orchestres d’harmonies, dont le NZ National Youth Concert Band et le 7th Wellington and Hawkes Bay Battalion Band où elle marqua l’histoire en tant que première femme admise.

Après avoir terminé ses études en Comédie Musicale au Wellington Performing Arts Centre, elle vint en France où elle décida de se diversifier en jouant des styles de musiques du monde, et musiques actuelles.

Elle se produisit à plusieurs reprises pour des membres de la famille royale britannique, dont la reine. Elle se produisit aussi aux côtés de l’orchestre symphonique de Nouvelle-Zélande pour les concerts d’Opera in the Park au célèbre terrain de cricket le Basin Reserve . En France, certains de ses moments forts de concertsinclurent les festivals Les Mediteraneos, Nancy Jazz Pulsation, Terre de Couleurs et Le Bourgeois Gentilhomme avec la compagnie Grenier de Toulouse. 

## Contact & Accès

```
École de Musique de Cazères - Maison Pour Tous
Rue des capucins
31220 CAZERES-SUR-GARONNE

email : ecoledemusique @ mptcazeres.fr
tél. MPT : 05.61.90.20.72
tél.: 06.88.62.24.32
```

### Les salles

Les salles de musique, mises à disposition par la commune de Cazères, se situent à proximité de l’école des Capucins. Ces salles sont proches les unes des autres.

![](images/2022-04-06-10-04-28.png)